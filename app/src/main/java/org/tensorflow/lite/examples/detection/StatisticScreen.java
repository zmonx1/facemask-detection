package org.tensorflow.lite.examples.detection;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.tensorflow.lite.examples.detection.model.StatisticsDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class StatisticScreen extends AppCompatActivity implements View.OnClickListener{
    TextView companyName , branchName , with_mask , without_mask , incorrect_mask , date , total;
    String numOfWithMask = null , numOfWithoutMask = null , numOfIncorrect = null ;
    EditText selectdate;
    PieChart pieChart;
    PieData pieData;
    List<PieEntry> pieEntryList = new ArrayList<>();

    private String dateforsql , currentDate;
    private int mYear, mMonth, mDay ,mdate;
    private StatisticsDatabase db = StatisticsDatabase.getInstance(this);
    public static final int[] COLORFUL_COLORS = {
            Color.rgb(71, 223, 6), Color.rgb(255, 153, 51), Color.rgb(255, 51, 51)

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistic_screen);

        companyName = findViewById(R.id.companyNames);
        branchName = findViewById(R.id.branchNames);

        with_mask = findViewById(R.id.correct);
        without_mask = findViewById(R.id.nomask);
        incorrect_mask = findViewById(R.id.incorrect);

        total = findViewById(R.id.total);
        date = findViewById(R.id.stringDate);

        SharedPreferences sp = getApplication().getSharedPreferences("MyUserPerfs", Context.MODE_PRIVATE);
        String comName = sp.getString("companyName", "");
        String braName = sp.getString("branchName", "");

        companyName.setText("ชื่อสถานที่ : "+comName);
        branchName.setText("สาขา : "+braName);

        selectdate = findViewById(R.id.edtdate);
        selectdate.setOnClickListener(this);

        pieChart = findViewById(R.id.pieChart);
        pieChart.setUsePercentValues(true);

    }
    @Override
    public void onClick(View v) {
        if (v == selectdate) {
            final Calendar calendar = Calendar.getInstance();
            mYear = calendar.get(Calendar.YEAR);
            mMonth = calendar.get(Calendar.MONTH);
            mDay = calendar.get(Calendar.DAY_OF_MONTH);
            int day = calendar.get(Calendar.DAY_OF_WEEK);

            //show dialog
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    int mm = (month+1);
                    int dd = (dayOfMonth);
                    dateforsql = String.valueOf(year+""+ String.format("%02d",mm)+""+String.format("%02d",dayOfMonth));
                    selectdate.setText( dayOfMonth+" /  " + (month+1) +" / "+year);
                        try {
                            pieEntryList.clear();
                            db.statisticsDao().getClassByDate(dateforsql);
                            numOfWithMask = String.valueOf(db.statisticsDao().getClassByDate(dateforsql).withMask);
                            numOfWithoutMask = String.valueOf(db.statisticsDao().getClassByDate(dateforsql).withoutMask);
                            numOfIncorrect = String.valueOf(db.statisticsDao().getClassByDate(dateforsql).maskIncorrect);
                            with_mask.setText("จำนวนการใส่หน้ากากถูกต้อง : " + numOfWithMask + " ครั้ง");
                            without_mask.setText("จำนวนไม่สวมใส่หน้ากาก : " + numOfWithoutMask + " ครั้ง");
                            incorrect_mask.setText("จำนวนการใส่หน้ากากไม่ถูกต้อง : " + numOfIncorrect + " ครั้ง");

                            int totalClass = (db.statisticsDao().getClassByDate(dateforsql).withMask) + (db.statisticsDao().getClassByDate(dateforsql).withoutMask) + (db.statisticsDao().getClassByDate(dateforsql).maskIncorrect);
                            total.setText("จำนวนการเข้าใช้งานทั้งหมด : " + String.valueOf(totalClass) + " ครั้ง");

                            pieEntryList.add(new PieEntry(db.statisticsDao().getClassByDate(dateforsql).withMask, "Correctly"));
                            pieEntryList.add(new PieEntry(db.statisticsDao().getClassByDate(dateforsql).maskIncorrect, "Incorrectly"));
                            pieEntryList.add(new PieEntry(db.statisticsDao().getClassByDate(dateforsql).withoutMask, "No mask"));
                            PieDataSet pieDataSet = new PieDataSet(pieEntryList, "");
                            pieDataSet.setColors(COLORFUL_COLORS);
                            pieData = new PieData(pieDataSet);
                            pieChart.setData(pieData);
                            pieChart.invalidate();

                        }catch (Exception e){
                            Toast.makeText(StatisticScreen.this, "ไม่มีข้อมูลของวันที่เลือก", Toast.LENGTH_LONG).show();
                            with_mask.setText("ไม่มีข้อมูลของวันที่ :  "+ dayOfMonth+" /  " + (month+1) +" / "+year);
                            without_mask.setText("");
                            incorrect_mask.setText("");
                            total.setText("");
                            pieEntryList.clear();
                            PieDataSet pieDataSet = new PieDataSet(pieEntryList, "");
                            pieData = new PieData(pieDataSet);
                            pieChart.setData(pieData);
                            pieChart.invalidate();
                        }
                    }
            }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
    }
}