package org.tensorflow.lite.examples.detection;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import org.tensorflow.lite.examples.detection.model.Statistics;
import org.tensorflow.lite.examples.detection.model.StatisticsDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;

public class HomeScreen extends AppCompatActivity {

    SharedPreferences sp;
    private int fristTimesCheck = 0;
    private static final String PREFS_NAME = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        ImageView btnRealtime = (ImageView) findViewById(R.id.btn_realtime);
        ImageView btnStatistics = (ImageView) findViewById(R.id.btn_statistics);
        ImageView btnSetting = (ImageView) findViewById(R.id.btnSetting);

        btnRealtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeScreen.this,DetectorActivity.class );
                startActivity(intent);
            }
        });

        btnStatistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeScreen.this,StatisticScreen.class );
                startActivity(intent);
            }
        });

        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeScreen.this,EditScreen.class );
                startActivity(intent);
            }
        });



        sp = getSharedPreferences(PREFS_NAME, 0);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String currentDate = sdf.format(new Date());
        if (!sp.getString("LAST_LAUNCH_DATE","nodate").contains(currentDate)){
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("LAST_LAUNCH_DATE", currentDate);
            editor.commit();
            Log.d("TAG", "onCreate: Date not matches. User has already Launched the app once today. : "+currentDate);

            StatisticsDatabase db = StatisticsDatabase.getInstance(this);
            Statistics statistics = new Statistics(0,0,0,currentDate);
            db.statisticsDao().insertAll(statistics);


        }else {
            Log.d("TAG", "sadasdsadsadsdsdasdasdsada "+currentDate);
            StatisticsDatabase db = StatisticsDatabase.getInstance(this);
            db.statisticsDao().getStatisList();
            Log.d("TAG", "onCreate: "+db.statisticsDao().getStatisList().get(0).maskIncorrect);
        }





    }
}