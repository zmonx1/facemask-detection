package org.tensorflow.lite.examples.detection;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EditScreen extends AppCompatActivity {

    EditText companyName , branchName;
    Button btnEdit;
    String companyNameStr , branchNameStr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_screen);

        companyName = findViewById(R.id.EcompanyName);
        branchName = findViewById(R.id.EbranchName);

        SharedPreferences sp = getApplication().getSharedPreferences("MyUserPerfs", Context.MODE_PRIVATE);
        String comName = sp.getString("companyName", "");
        String braName = sp.getString("branchName", "");

        companyName.setText(comName);
        branchName.setText(braName);

        btnEdit =findViewById(R.id.btn_edit);

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                companyNameStr = companyName.getText().toString();
                branchNameStr = branchName.getText().toString();
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("companyName", companyNameStr);
                editor.putString("branchName", branchNameStr);
                editor.commit();
                Toast.makeText(EditScreen.this, "บันทึกข้อมูลสำเร็จ", Toast.LENGTH_LONG).show();

            }
        });
    }
}