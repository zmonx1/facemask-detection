package org.tensorflow.lite.examples.detection;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisScreen extends AppCompatActivity {

    private EditText companyName;
    private EditText branchName;
    private Button btnFinish;
    SharedPreferences sp;
    String companyNameStr, branchNameStr;
    private final Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regis_screen);

        companyName = (EditText) findViewById(R.id.companyName);
        branchName = (EditText) findViewById(R.id.branchName);
        btnFinish = (Button) findViewById(R.id.btnFinish);

        sp = getSharedPreferences("MyUserPerfs", Context.MODE_PRIVATE);

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                companyNameStr = companyName.getText().toString();
                branchNameStr = branchName.getText().toString();

                SharedPreferences.Editor editor = sp.edit();
                editor.putString("companyName", companyNameStr);
                editor.putString("branchName", branchNameStr);
                editor.commit();
                Toast.makeText(RegisScreen.this, "บันทึกข้อมูลสำเร็จ", Toast.LENGTH_LONG).show();

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(RegisScreen.this, HomeScreen.class);
                        startActivity(intent);
                    }
                }, 1000);
            }
        });
    }
}