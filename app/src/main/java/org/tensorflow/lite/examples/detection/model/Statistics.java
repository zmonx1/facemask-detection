package org.tensorflow.lite.examples.detection.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import java.util.Date;

@Entity (tableName = "statistics")
public class Statistics {
    @PrimaryKey(autoGenerate = true)
    public int sid;

    @ColumnInfo(name = "withmask")
    public int withMask;

    @ColumnInfo(name = "without_Mask")
    public int withoutMask;

    @ColumnInfo(name = "maskIncorrect")
    public int maskIncorrect;

    @ColumnInfo(name = "date")
    public String date;


    public Statistics(int sid, int withMask, int withoutMask, int maskIncorrect, String date) {
        this.sid = sid;
        this.withMask = withMask;
        this.withoutMask = withoutMask;
        this.maskIncorrect = maskIncorrect;
        this.date = date;
    }

    @Ignore
    public Statistics(int withMask, int withoutMask, int maskIncorrect, String date) {
        this.withMask = withMask;
        this.withoutMask = withoutMask;
        this.maskIncorrect = maskIncorrect;
        this.date = date;
    }
}
