package org.tensorflow.lite.examples.detection.model;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface StatisticsDao {

    @Query("SELECT * FROM Statistics")
    List<Statistics> getStatisList();

    @Query("SELECT * FROM Statistics WHERE date LIKE :date")
    Statistics getClassByDate(String date);


    @Query("UPDATE Statistics SET withMask= :numOfwithmask WHERE date LIKE :date")
    void coutWithmask(int numOfwithmask , String date);

    @Query("UPDATE Statistics SET without_Mask= :numOfwithout_Mask WHERE date LIKE :date")
    void coutwithout_Mask(int numOfwithout_Mask , String date);

    @Query("UPDATE Statistics SET maskIncorrect= :numOfmaskIncorrect WHERE date LIKE :date")
    void coutmaskIncorrect(int numOfmaskIncorrect , String date);

    @Insert
    void insertAll(Statistics statistics);

    @Update
    void updateAll(Statistics statistics);
}
