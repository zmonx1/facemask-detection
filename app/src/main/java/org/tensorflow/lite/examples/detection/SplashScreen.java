package org.tensorflow.lite.examples.detection;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;

public class SplashScreen extends AppCompatActivity {
    private final Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        SharedPreferences sp = getApplication().getSharedPreferences("MyUserPerfs", Context.MODE_PRIVATE);
        String comName = sp.getString("companyName", "");

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (comName.length() == 0) {
                    Intent intent = new Intent(SplashScreen.this, RegisScreen.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(SplashScreen.this, HomeScreen.class);
                    startActivity(intent);
                }
            }
        }, 1000);
    }
}