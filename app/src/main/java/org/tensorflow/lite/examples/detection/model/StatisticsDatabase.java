package org.tensorflow.lite.examples.detection.model;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Statistics.class} , exportSchema = false,version = 1)

public abstract class StatisticsDatabase extends RoomDatabase {

    private static final String DB_name = "mask_db";
    private static StatisticsDatabase instance;

    public static synchronized StatisticsDatabase getInstance (Context context){
        if (instance == null){
                instance = Room.databaseBuilder(context.getApplicationContext(), StatisticsDatabase.class,DB_name)
                        .allowMainThreadQueries()
                        .fallbackToDestructiveMigration()
                        .build();
        }
        return instance;
    }

    public abstract StatisticsDao statisticsDao();

}
